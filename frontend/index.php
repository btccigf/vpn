<?php
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


# expect confirmation
if (!$_REQUEST["confirm"]) {
   echo "<html><body><a href=\"".$SERVER_['REQUEST_URI']."?confirm=1\">Confirmer vouloir bannir le VPN actuellement actif</a></body></html>";
   exit;

}

# set 1 into vpn-ban.txt - vpn-ban must belong to www-data so it has
# write access
$file = fopen("vpn-ban.txt", "w");
fwrite($file, "1");
fclose($file);
echo "Changement de VPN d'ici une minute.";

?>