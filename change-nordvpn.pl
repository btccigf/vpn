#!/usr/bin/perl
#
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>


use strict;
use Fcntl qw(:flock);
use Sys::Syslog;
use Capture::Tiny qw(capture); 
use Text::Diff;
    
# debian packages:
#    libio-interface-perl libcapture-tiny-perl libtext-diff-perl

###########  conf

my $conffile = "/etc/openvpn/nordvpn.conf";
my $conffile_password = "/etc/openvpn/nordvpn.pass"; # first line = user, next = password
my $conffile_available = "/etc/openvpn/ovpn_tcp/fr*.ovpn";  # by default only french and luxembourg

###########  functions

# wrapper for system() calls to remove unwanted output garbage
sub systemc {
    my @cmd = @_;
    my ($stdout, $stderr, $exit) = capture { system(@cmd); };
    return $exit;
}

########## RUN

# silently forbid concurrent runs
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

# start logging
openlog("change-nordvpn.pl", "", "LOG_USER");

# get list of possible VPN config
my @ovpns = glob($conffile_available);

# exit if none found
syslog("LOG_WARNING", "no configuration file available ($conffile_available)") and exit
    unless scalar(@ovpns);

# otherwise, pick random (limit attempts to find a new one to 50)
my $conffile_selected;
for (my $i = 0; $i <= 50; $i++) {
    # random pick from the list
    $conffile_selected = @ovpns[int(rand(scalar(@ovpns)))];

    # end the loop if different from the current one
    last if diff($conffile, $conffile_selected);
}
syslog("LOG_WARNING", "selected $conffile_selected");

# next step, check if "auth-user-pass /etc/openvpn/nordvpn.pass" is properly
# included
open(CONFFILE, "< $conffile_selected");
my $conffile_selected_content;
my $conffile_selected_require_update = 0;
while (<CONFFILE>) {
    # look for auth-user-pass line
    if (/^auth-user-pass/) {
	# check if it include proper reference to password file
	unless (/^auth-user-pass $conffile_password$/) {
	    # if not, then set it now
	    $conffile_selected_content .= "# user/pass configuration file (each on one line)\nauth-user-pass $conffile_password\n";
	    # mark that we need to update the file
	    $conffile_selected_require_update++;
	    # and skip the line
	    next ;		
	}
    }
    # in other, keep the line as it is
    $conffile_selected_content .= $_;
}
close(CONFFILE);
# if required, then print rewrite the file
if ($conffile_selected_require_update) {
    open(CONFFILE, "> $conffile_selected");
    print CONFFILE $conffile_selected_content;
    close(CONFFILE);
    syslog("LOG_WARNING", "auth-user-pass set in $conffile_selected");
}

# finally, set properly the symbolic link
unlink($conffile);
system("/bin/ln", "-s", $conffile_selected, $conffile);
syslog("LOG_WARNING", "linked $conffile to $conffile_selected");


# EOF
