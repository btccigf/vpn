#!/usr/bin/perl
#
# (c) 2018 https://gitlab.com/mrigf
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>

use strict;
use Fcntl qw(:flock);
use Sys::Syslog;
use Capture::Tiny qw(capture); 
use Text::Diff;
    
# related to change-nordvpn, check if user asked via http://vrac/vpn/ the 
# current vpn to be banned

###########  conf

my $banfile = "/scratch/lxc/vrac/rootfs/opt/vpn/vpn-ban.txt";
my $conffile = "/etc/openvpn/nordvpn.conf";

########## RUN

# silently forbid concurrent runs
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

# check if change is required (1 in $banfile set by the PHP script)
my $requested = 0;
open(REQUEST, "< $banfile");
while (<REQUEST>) { $requested = 1 if m/1/; }
close(REQUEST);

# not requested? end here (with error code - anything but 0)
exit 1 unless $requested;

# start logging
openlog("ban-nordvpn.pl", "", "LOG_USER"); 

# otherwise identify the current VPN, which is to which file the $conffile
# symlink points to
my $conffile_current = readlink($conffile);

# log it
syslog("LOG_WARNING", "requested to ban $conffile_current");
print "retrait de la configuration $conffile_current demandé\n";

# remove it
system("mv", "-f", $conffile_current, $conffile_current.".off");

# reset the ban flag
open(REQUEST, "> $banfile");
print "";
close(REQUEST);


# now immediately, change change-nordvpn + rc-service openvpn reload should be
# called in order for the ban to take effect

# EOF
